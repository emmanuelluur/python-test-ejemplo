"""
Pruebas shopping cart
"""
import unittest
from shopping_cart import Producto, ShoppingCart, NotExists


class TestShoppingCart(unittest.TestCase):

    def setUp(self):
        self.pan = Producto('pan', 8)
        self.manzana = Producto('manzana', 15)
        self.shopping_cart = ShoppingCart()
        self.shopping_cart.add_item(self.pan)

    def tearDown(self):
        """ se ejecuta al terminar test """
        pass

    def test_cinco_mas_cinco_igual_diez(self):
        """ metodo prueba """
        assert 5 + 5 == 10

    def test_nombre_producto_igual_pan(self):
        self.assertEqual(self.pan.name, 'pan')

    def test_nombre_producto_no_manzana(self):
        self.assertNotEqual(self.manzana.name, 'platano')

    def test_cart_tiene_producto(self):
        self.assertTrue(self.shopping_cart.contain_items())

    def test_cart_no_tiene_producto(self):
        self.shopping_cart.remove_item(self.pan)
        self.assertFalse(self.shopping_cart.contain_items())

    def test_obtiene_pan(self):
        item = self.shopping_cart.get_item(self.pan)
        self.assertIs(item, self.pan)

    def test_excepcion_obtiene_manzana(self):
        with self.assertRaises(NotExists):
            self.shopping_cart.get_item(self.manzana)

    def test_total_suma_precios(self):
        total = self.shopping_cart.total()
        self.assertGreater(total, 0)
        self.assertEqual(total, self.pan.price)

    def test_codigo_producto(self):
        self.assertRegex(self.pan.code(), self.pan.name)

    def test_fail(self):
        if 2 > 3:
            self.fail("2 not > 3")


if __name__ == "__main__":
    unittest.main()
