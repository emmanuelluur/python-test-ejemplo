class NotExists(Exception):
    pass
class Producto(object):

    def __init__(self, name, price=0):
        self.name = name
        self.price = price
    
    def code(self):
        return "{}-123456789".format(self.name)
    def __str__(self):
        return self.name

class ShoppingCart(object):
    def __init__(self):
        self.items = []

    def add_item(self, item):
        self.items.append(item)
    def total(self):
        return sum([ item.price for item in self.items ])
    def remove_item(self, item):
        self.items.remove(item)
    def contain_items(self):
        return len(self.items) > 0
    def get_item(self, item):
        if item not in self.items:
            raise NotExists("Item not exists")
        else:   
            return self.items[ self.items.index(item) - 1 ]

if __name__ == "__main__":
    pass